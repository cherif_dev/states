
import { Action } from '@ngrx/store';
export enum CustomerActionTypes {
    Add = '[Customer Component] Add',
    Remove = '[Customer Component] Remove'
}

export class ActionEx implements Action {
    readonly type;
    payload: any;
}

export class CustomerAdd implements ActionEx {
    type = CustomerActionTypes.Add;
    payload: any;
    constructor(payload: any) {
        this.payload = payload;
    }
}

export class CustomerRemove implements ActionEx {
    type = CustomerActionTypes.Remove;
    payload: any;
    constructor(payload: any) {

    }

}
